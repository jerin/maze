#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <cassert>
using namespace std;

#define for_all(p, X) for((p)=(X).begin(); (p)!=(X).end(); (p)++)
#define debug(x) cerr<<(#x)<<':'<<(x)<<'\n';

struct cell{
    int i, j;
};

struct wall{
    cell c1, c2;
    int set;
};

class DSD{
    //Disjoint Set Data Structure
    int *P, *R;
    int size;
    public:
    DSD(int N){
        P = new int[N];
        R = new int[N];
        size = N;
        for(int i=0; i<N; i++){
            P[i] = i;
            R[i] = 0;
        }
    }

    ~DSD(){
        delete[] P;
        delete[] R;
    }


    int merge(int x, int y){
        int u, v;
        u = find(x);
        v = find(y);
        if(R[u]>R[v]){
            R[u] = R[v]+1;
            P[u] = v;
        }
        else{
            R[v] = R[u]+1;
            P[v] = u;
        }
    }

    int find(int x){
        if(x==P[x]) return x;
        P[x] = find(P[x]);
        return P[x];
    }
};

class maze{
    char **B;
    int H, L;
    vector<wall> EL;
    int *active;
    vector<wall> FEL;
    public:
    maze(int X, int Y){
        assert(X>=Y);
        L = X;
        H = Y;
    }

    void add_edges(){
        int i, j;
        for(i=0; i<L; i++){
            for(j=0; j<H; j++){
                if(i!=L-1){
                    EL.push_back((wall){(cell){i, j},  (cell){i+1, j}, 1});
                }
                if(j!=H-1){
                    EL.push_back((wall){(cell){i, j},  (cell){i, j+1}, 1});
                }
            }
        }
    }

    int h(cell e){
        //Hashing cell into N*N space, for DSD;
        return e.i*L+e.j;
    }

    void gen_maze(){
        //Generate maze using Kruskal's Algorithm
        random_shuffle(EL.begin(), EL.end());
        vector<wall>::iterator p;
        DSD D(H*L);
        int c1, c2;
        int s, e;
        for_all(p, EL){
            c1 = D.find(h(p->c1));
            c2 = D.find(h(p->c2));
            s = D.find(h((cell){0,0}));
            e = D.find(h((cell){L-1, H-1}));
            if(s == e){
                if(!(c1 == s && c2 == s)){
                    D.merge(h(p->c1), h(p->c2));
                    FEL.push_back(*p);
                    p->set = 0;
                    
                }
            }
            else if(c1 != c2){
                //Remove wall, change in board.
                D.merge(h(p->c1), h(p->c2));
                FEL.push_back(*p);
                p->set = 0;
            }
            else{
                continue;
            }
        }
    }

    void print_maze(){
        char Board[2*L+1][2*H+1];
        vector<wall>::iterator p;
        int i_, j_, i, j;
        for(i=0; i<L; i++){
            for(j=0; j<H; j++){
                i_ = 2*i+1;
                j_ = 2*j+1;
                Board[i_][j_] = ' ';
                Board[i_-1][j_] = '-';
                Board[i_+1][j_] = '-';
                Board[i_][j_-1] = '|';
                Board[i_][j_+1] = '|';
                Board[i_+1][j_+1] = '+';
                Board[i_-1][j_+1] = '+';
                Board[i_+1][j_-1] = '+';
                Board[i_-1][j_-1] = '+';
            }
        }
        for_all(p, FEL){
            i = p->c1.i;
            j = p->c1.j;
            i_ = p->c2.i;
            j_ = p->c2.j;
            Board[i+i_+1][j+j_+1] = ' ';
        }
        Board[1][0] = ' ';
        Board[2*L-1][2*H] = ' ';
        for(i=0; i<2*L+1; i++){
            for(j=0; j<2*H+1; j++){
                cout<<Board[i][j];
            }
            cout<<'\n';
        }
    }

    void gen_xy(const char *filename){
        int i, j, x, y;
        FILE *f = fopen(filename, "w+");
        vector<wall>::iterator p;
        for_all(p, EL){
            if(p->set){
                if(p->c1.i == p->c2.i){
                    fprintf(f, "%d %d\n%d %d\n\n", p->c1.i, p->c2.j, p->c2.i+1, p->c2.j);
                }
                else{
                    fprintf(f, "%d %d\n%d %d\n\n", p->c2.i, p->c1.j, p->c2.i, p->c2.j+1);
                }
            }
        }
        fprintf(f, "%d %d\n%d %d\n\n", 0, H, 0, 1);
        fprintf(f, "%d %d\n%d %d\n\n", L, 0, 0, 0);
        fprintf(f, "%d %d\n%d %d\n\n", L, H-1, L, 0);
        fprintf(f, "%d %d\n%d %d\n\n", 0, H, H, L);
        fclose(f);
    }

    void solve_maze(){

    }
};



int main(){
    srand(time(NULL));
    int H, L;
    cin>>L>>H;
    maze m(L, H);
    m.add_edges();
    m.gen_maze();
    m.print_maze();
    m.gen_xy("gp.in");
    return 0;
}
