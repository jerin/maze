var dsd = function (size) {
    this.N = size;
    this.P = new Array(this.N);
    this.R = new Array(this.N);

    this.init = function () {
        for (var i = 0; i < this.N; i++) {
            this.P[i] = i;
            this.R[i] = 0;
        }
    }

    this.union = function (x, y) {
        var u = this.find(x);
        var v = this.find(y);
        if (this.R[u] > this.R[v]) {
            this.R[u] = this.R[v] + 1;
            this.P[u] = v;
        }
        else {
            this.R[v] = this.R[u] + 1;
            this.P[v] = u;
        }
    }

    this.find = function (x) {
        if (x == this.P[x])
            return x;
        this.P[x] = this.find(this.P[x]);
        return this.P[x];
    }
};

var maze = function (M, N, S) {
    this.N = N;
    this.M = M;
    this.S = S;
    this.Board = new Array(2 * this.M + 1);
    this.EL = new Array();
    this.vis = new Array(2 * this.M + 1);
    this.delay = 2;
    this.x = 1;
    this.init = function () {
        for (var i = 0; i < 2 * this.M + 1; i++) {
            this.Board[i] = new Array(2 * this.N + 1);
            this.vis[i] = new Array(2 * this.N + 1);
        }

        for (var i = 0; i < 2 * this.M + 1; i++) {
            for (var j = 0; j < 2 * this.N + 1; j++) {
                if (!(i % 2) && !(j % 2)) {
                    this.Board[i][j] = '+';
                }
                else if (!(i % 2)) {
                    this.Board[i][j] = '-';
                }
                else if (!(j % 2)) {
                    this.Board[i][j] = '|';
                }
                else {
                    this.Board[i][j] = ' ';
                }
                this.vis[i][j] = 0;
            }
        }
    }
    this.add_edges = function () {
        for (var i = 0; i < this.M; i++) {
            for (var j = 0; j < this.N; j++) {
                if (i != this.M - 1) {
                    this.EL.push([[i, j], [i + 1, j], 1]);
                }
                if (j != this.N - 1) {
                    this.EL.push([[i, j], [i, j + 1], 1]);
                }
            }
        }
    }
    //Hash function
    this.h = function (e) {
        return e[0] * this.N + e[1];
    }
    this.randomize = function (EL) {
        for (var i = 0; i < EL.length; i++) {
            var si = Math.floor(Math.random() * 387) % EL.length;
            var tmp = EL[si];
            EL[si] = EL[i];
            EL[i] = tmp;
        }
        return EL;
    }
    this.breakwall = function (e) {
        var x = e[0][0] + e[1][0] + 1;
        var y = e[0][1] + e[1][1] + 1;
        this.Board[x][y] = ' ';
    }
    this.gen_maze = function () {
        this.EL = this.randomize(this.EL);
        var D = new dsd(this.N * this.M);
        D.init();
        var s = this.h([0, this.N-1]);
        var e = this.h([this.M - 1, 0]);
        this.Board[0][1] = ' ';
        this.current_cell = [0, 1];
        this.Board[2*this.M][2 * this.N-1] = ' ';
        //Run Kruskal
        for (var i = 0; i < this.EL.length; i++) {
            var x = this.h(this.EL[i][0]);
            var y = this.h(this.EL[i][1]);


            if (D.find(s) == D.find(e)) {
                if (!(D.find(x) == D.find(s) && D.find(y) == D.find(s))) {
                    if (D.find(x) != D.find(y)) {
                        D.union(x, y);
                        this.breakwall(this.EL[i]);
                        this.EL[i][2] = 0;
                    }

                }
            }

            else if (D.find(x) != D.find(y)) {
                D.union(x, y);
                this.breakwall(this.EL[i]);
                this.EL[i][2] = 0;
            }
            else {
                continue;
            }
        }

    }

    this.draw_canvas = function (id) {
        this.canvas = document.getElementById(id);
        var scale = this.S;
        if (this.canvas.getContext) {
            this.ctx = this.canvas.getContext('2d');
            for (var i = 0; i < 2 * this.M + 1; i++) {
                for (var j = 0; j < 2 * this.N + 1; j++) {
                    if (this.Board[i][j] != ' ') {
                        this.ctx.fillRect(scale * i, scale * j, scale, scale);
                    }
                }
            }
        }
    }

    this.setcolor = function (e, color) {
        var scale = this.S;
        if (color === "green") {
            this.ctx.fillStyle = "rgb(0, 255, 0)";
        }
        if (color === "red") {
            this.ctx.fillStyle = "rgb(255, 0, 0)";
        }
        
        if (color ==="blue"){
            this.ctx.fillStyle = "rgb(0, 0, 255)";
        }
        this.ctx.fillRect(scale * e[0], scale * e[1], scale, scale);
        this.ctx.fillStyle = "rgb(0, 0, 0)";
    }

    this.dfs = function (e) {
        if (e[0] < 0 || e[1] < 0) {
            return false;
        }
        if (e[0] > 2 * this.M || e[1] > 2 * this.N) {
            return false;
        }
        if (this.vis[e[0]][e[1]]) {
            return false;
        }
        this.vis[e[0]][e[1]] = 1;

        if (this.Board[e[0]][e[1]] != ' ') {
            return false;
        }
        if (e[0] == 2 * this.N - 1 && e[1] == 2 * this.M) {
            this.setcolor(e, 'green');
            return true;
        }

        var self = this;
        setTimeout(function () { self.setcolor(e, 'green') }, this.delay * (this.x++));

        if (this.dfs([e[0] + 1, e[1]])
                || this.dfs([e[0], e[1] + 1])
                || this.dfs([e[0] - 1, e[1]])
                || this.dfs([e[0], e[1] - 1])
           ) {
            return true;
        }
        else {
            var self = this;
            setTimeout(function () { self.setcolor(e, 'red') }, this.delay * (this.x++));
            return false;
        }
    }

    this.solve = function () {
        var s = [1, 0];
        this.setcolor(s, 'green');
        this.dfs(s);
    }

    this.try_move = function(x, y){
        c = this.current_cell;
        if(c[0]+x<0 || c[1]+y<0)
            return false;
        if(c[0]+x>2*this.M || c[1]>2*this.N)
            return false;
        if(this.Board[c[0]+x][c[1]+y]!=' '){
            return false;
        }
        if(this.vis[c[0]+x][c[1]+y]){
            this.setcolor(c, 'red')
        }
        else{
            this.setcolor(c, 'green');
        }
        this.vis[c[0]+x][c[1]+y] = 1;
        this.setcolor([c[0]+x, c[1]+y], 'blue');
        c[0] += x;
        c[1] += y;
        if(c[0] ==  2*this.M && c[1] == 2*this.N-1){
            //completion code goies here.
            //alert("Congratulations");
            return true;
        }
        return false;
    }
};



//Driver

m = new maze(8 , 7, 60);
m.init();
m.add_edges();
m.gen_maze();
m.draw_canvas("c");

canvas = document.getElementById("c");
ctx = canvas.getContext("2d");
function f(e){
    console.log(e.key);
    if(e.key == 'Up'){
        m.try_move(0, -1);
    }
    else if(e.key == 'Down'){
        m.try_move(0, 1);
    }
    else if(e.key == 'Right'){
        m.try_move(1, 0);
    }
    else if(e.key == 'Left'){
        m.try_move(-1, 0);
    }
}


document.addEventListener("keypress", f, false);
